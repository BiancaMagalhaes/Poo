package ucsal.br.poo20182.bes;

import java.awt.HeadlessException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;


import javax.swing.JOptionPane;

public class ExercicioLista04 {

	public static void main(String[] args) throws HeadlessException, ParseException {
		executar();
	}

	public static void executar() throws HeadlessException, ParseException {

		// Variaveis

		DateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		String[] menu = { "Incluir contato", "Excluir contato", "Listar contatos", "Pesquisar contato" };
		
		String[] contato = new String[4];
		Contatos contato0 = new Contatos();
		Contatos contato1 = new Contatos();
		Contatos contato2 = new Contatos();
		Contatos contato3 = new Contatos();
		Object[] alocamento = { contato0, contato1, contato2, contato3 };
		
	

		// Entrada

		JOptionPane.showMessageDialog(null, "Seja bem-vindo a sua agenda!", "Agenda", JOptionPane.INFORMATION_MESSAGE);
		operacoes(format, menu, contato, contato0, alocamento);

		// Saida
		


	}

	public static void novaOperacao(DateFormat format, String[] menu, String[] contato, Contatos contato0,
			Object[] alocamento) throws ParseException {
		int respPerg;
		String [] sn = {"SIM", "N�O"};
		
		respPerg = JOptionPane.showOptionDialog(null, "Deseja realizar outro procedimento?", "Agenda",
				JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.INFORMATION_MESSAGE, null, sn, 0);
		if(respPerg == 0) {
			operacoes(format, menu, contato, contato0, alocamento);
		}else {
			JOptionPane.showMessageDialog(null, "At� a pr�xima!", "Agenda", JOptionPane.INFORMATION_MESSAGE);
		}
	}

	public static void operacoes(DateFormat format, String[] menu, String[] contato, Contatos contato0,
			Object[] alocamento) throws ParseException {
		int cont = 0;
		int ct = 0;
		int respMenu;
		String nome;
		respMenu = JOptionPane.showOptionDialog(null, "Escolha a a��o que deseja realizar", "Agenda",
				JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.INFORMATION_MESSAGE, null, menu, 0);

		// Processamento
		switch (respMenu) {
		case 0:
			contato0.nome = JOptionPane.showInputDialog("Nome do contato:");
			contato[cont] = contato0.nome;
			cont++;
			contato0.telefone = JOptionPane.showInputDialog("Telefone do contato:");
			contato0.anoNascimento = Integer.parseInt(JOptionPane.showInputDialog("Ano do nascimento do contato:"));
			contato0.dataNascimento = format.parse(JOptionPane.showInputDialog("Data de nascimento do contato:"));
			break;
		case 1:
			ct = 0;
			nome = JOptionPane.showInputDialog("Digite o nome do contato que deseja apagar:");
			while (ct < 4) {
				if (nome == contato[ct]) {
					System.out.println(contato[ct]);
					alocamento[ct] = null;
					System.out.println(contato[ct]);
				}
				ct++;
			}
			JOptionPane.showMessageDialog(null, "Contato apagado", "Agenda", JOptionPane.INFORMATION_MESSAGE);
			break;
		case 2:
			JOptionPane.showMessageDialog(null, contato, "Contatos", JOptionPane.INFORMATION_MESSAGE);
			break;
		case 3:
			cont = 0;
			nome = JOptionPane.showInputDialog("Digite o nome do contato que deseja pesquisar:");
			while (cont < alocamento.length) {
				if (nome == contato[cont]) {
					JOptionPane.showMessageDialog(null, alocamento[cont], "Contato", JOptionPane.INFORMATION_MESSAGE);
				}
				cont++;
			}
			break;
		}
		novaOperacao(format, menu, contato, contato0, alocamento);
	}

}
