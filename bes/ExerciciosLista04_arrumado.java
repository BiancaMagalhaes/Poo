package ucsal.br.poo20182.bes;

import java.awt.HeadlessException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;


import javax.swing.JOptionPane;

public class ExerciciosLista04_arrumado {

	public static void main(String[] args) throws HeadlessException, ParseException {
		executar();
	}

	public static void executar() throws HeadlessException, ParseException {

		// Variaveis
		SimpleDateFormat date = new SimpleDateFormat("dd/MM/yyyy");
		ArrayList<String> contatos = new ArrayList<String>();
		Contatos contato0 = new Contatos();
		Contatos contato1 = new Contatos();
		Contatos contato2 = new Contatos();
		Contatos contato3 = new Contatos();
		int cont = 0;
		String[] menu = { "Incluir contato", "Excluir contato", "Listar contatos", "Pesquisar contato" };

		// Object[] alocamento = { contato0, contato1, contato2, contato3 };

		// Entrada

		JOptionPane.showMessageDialog(null, "Seja bem-vindo a sua agenda!", "Agenda", JOptionPane.INFORMATION_MESSAGE);
		// operacoes(format, menu, contato, contato0, alocamento);
		operacoes(date, menu, contatos, contato0, contato1, contato2, contato3, cont);

		// Saida

	}

	public static void operacoes(SimpleDateFormat date, String[] menu, ArrayList<String> contatos, Contatos contato0,
			Contatos contato1, Contatos contato2, Contatos contato3, int cont)
			throws ParseException {
		int respMenu;
		respMenu = JOptionPane.showOptionDialog(null, "Escolha a a��o que deseja realizar", "Agenda",
				JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.INFORMATION_MESSAGE, null, menu, 0);

		switch (respMenu) {
		case 0:
			if (cont == 0) {
				contato0 = inserirContato(date, contatos);
			}
			if (cont == 1) {
				contato1 = inserirContato(date, contatos);
			}
			if (cont == 2) {
				contato2 = inserirContato(date, contatos);
			}
			if (cont == 3) {
				contato3 = inserirContato(date, contatos);
			}
			cont++;
			break;
		case 1:
			int ct;
			ct = excluirContato(contatos);
			if (ct == 0) {
				contato0 = null;
			}
			if (ct == 1) {
				contato1 = null;
			}
			if (ct == 2) {
				contato2 = null;
			}
			if (ct == 3) {
				contato3 = null;
			}
			break;
		case 2:
			listarContatos(contatos);
			break;
		case 3:
			int conta;
			conta = pesquisarContatos(contatos);
			if (conta == 0) {
				mostrarContato(contato0);
			}
			if (conta == 1) {
				mostrarContato(contato1);
			}
			if (conta == 2) {
				mostrarContato(contato2);
			}
			if (conta == 3) {
				mostrarContato(contato3);
			}else{
			break;
			
		}
		}
		novoProcedimento(date, menu, contatos, contato0, contato1, contato2, contato3, cont);
	}

	public static Contatos inserirContato(SimpleDateFormat date, ArrayList<String> contatos) throws ParseException {
		Contatos contato = new Contatos();

		contato.nome = JOptionPane.showInputDialog("Nome do contato:");
		contatos.add(contato.nome);
		contato.telefone = JOptionPane.showInputDialog("Telefone do contato:");
		contato.anoNascimento = Integer.parseInt(JOptionPane.showInputDialog("Ano do nascimento do contato:"));
		contato.dataNascimento = date.parse(JOptionPane.showInputDialog("Data de nascimento do contato:(XX/XX/XXXX)"));
		return contato;
	}

	public static int excluirContato(ArrayList<String> contato) {
		String nome = JOptionPane.showInputDialog("Digite o nome do contato que deseja apagar:");
		for (int i = 0; i < contato.size(); i++) {
			if (nome == contato.get(i)) {
				JOptionPane.showMessageDialog(null, "Contato apagado", "Agenda", JOptionPane.INFORMATION_MESSAGE);
				return i;
			} else {
				JOptionPane.showMessageDialog(null, "Contato n�o existe");
			}
		}
		return 9;
	}

	public static void listarContatos(ArrayList<String> contato) {
		JOptionPane.showMessageDialog(null, contato.toArray(), "Contatos", JOptionPane.INFORMATION_MESSAGE);
	}

	public static int pesquisarContatos(ArrayList<String> contato) {
		String nome = JOptionPane.showInputDialog("Digite o nome do contato que deseja pesquisar:");
		for (int i = 0; i < contato.size(); i++) {
			if (contato.get(i).equals(nome)){
				return i;
			} else {
				JOptionPane.showMessageDialog(null, "Contato n�o existe");
			}
		}
		return 9;
	}

	public static void mostrarContato(Contatos contato) {
		JOptionPane.showMessageDialog(null, contato, "Contatos", JOptionPane.INFORMATION_MESSAGE);

	}

	public static void novoProcedimento(SimpleDateFormat date, String[] menu, ArrayList<String> contatos, Contatos contato0,
			Contatos contato1, Contatos contato2, Contatos contato3, int cont) throws ParseException {
		int respPerg;
		String[] sn = { "SIM", "N�O" };

		respPerg = JOptionPane.showOptionDialog(null, "Deseja realizar outro procedimento?", "Agenda",
				JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.INFORMATION_MESSAGE, null, sn, 0);
		if (respPerg == 0) {
			operacoes(date, menu, contatos, contato0, contato1, contato2, contato3, cont);
		} else {
			JOptionPane.showMessageDialog(null, "At� a pr�xima!", "Agenda", JOptionPane.INFORMATION_MESSAGE);
		}
	}

}